package main

import (
	"net/http"

	routes "gitlab.com/wegAndrade/lojago/Routes"
)

func main() {
	routes.CarregaRotas()
	http.ListenAndServe(":8000", nil)
}

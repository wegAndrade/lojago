package data

import (
	"fmt"

	models "gitlab.com/wegAndrade/lojago/Models"
)

func CriarNovoProduto(nome string, descricao string, quantidade int, preco float64) {
	db := ConectaComBancoDeDados()
	insereDadosBanco, err := db.Prepare("INSERT INTO PRODUTOS (nome, descricao,quantidade,preco) values ($1,$2,$3,$4)")
	if err != nil {
		panic(err.Error())
	}
	insereDadosBanco.Exec(nome, descricao, preco, quantidade)
	defer db.Close()
}
func ObterProdutos() []models.Produto {
	db := ConectaComBancoDeDados()
	p := models.Produto{}
	produtos := []models.Produto{}
	selectDeTodosProdutos, err := db.Query("Select * from Produtos")
	if err != nil {
		fmt.Printf("panic: %v\n", err)
	}
	for selectDeTodosProdutos.Next() {
		var id, quantidade int
		var descricao, nome string
		var preco float64
		err = selectDeTodosProdutos.Scan(&id, &nome, &descricao, &preco, &quantidade)
		if err != nil {
			fmt.Printf("panic: %v\n", err)
		}
		p.Id = id
		p.Nome = nome
		p.Descricao = descricao
		p.Preco = preco
		p.Quantidade = quantidade

		produtos = append(produtos, p)
	}
	defer db.Close()
	return produtos
}
